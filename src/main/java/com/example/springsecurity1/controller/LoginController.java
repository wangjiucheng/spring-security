package com.example.springsecurity1.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class LoginController {

    @RequestMapping("login")
    public String login(){
        log.info("进入登录页面");
        return "redirect:main.html";
    }
}
